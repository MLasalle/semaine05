#include "stdio.h"
#include "string.h"

int main(int argc, char const *argv[])
{
	int len = 300;
	char buffer[len];
	FILE *f = fopen(argv[1], "r");

	if(!f) return 1;

	char *ville, *pays, *pop;
	char *reste;
	int nbVille = 0;


	printf("%-s %-15s %-s %-s\n", "Rang", "Nom", "Pays", "Population");
	printf("%-s %-15s %-s %-s\n", "----", "---", "----", "----------");

	while(fgets(buffer, len, f) != NULL){
		ville = strtok_r(buffer, ",",&reste);
		pays = strtok_r(reste, ",",&reste);
		pop = strtok_r(reste, "\n",&reste);

		printf("%04d %-15s %-.4s %10s\n", ++nbVille, ville, pays, pop);
	}

	fclose(f);
	return 0;
}
