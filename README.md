# Solution laboratoire 5 | 16 octobre 2018

[Énoncé du laboratoire](https://gitlab.com/Morriar/inf3135-laboratoires/blob/master/semaine05.md)

## Utilisation
```shell
$ make clean
$ make
$ ./villes villes.txt
$ ./divise | dot -Tpdf -o divise.pdf
$ ./queue
```

## Commandes d'une ligne

1.
```shell
$ man printf | head > man-printf.txt
```
2.
```shell
$ curl https://gitlab.com/Morriar/inf3135-laboratoires/raw/master/exemples/array.c | grep //
```
3.
```shell
$ git clone https://github.com/baylej/tmx
$ cd tmx
$ $git log | grep Author | sort | uniq
```
4.
```shell
$ git log | grep Author | sed 's/Author: //' | sed 's/<.*//' | sort | uniq
```

## Ressources
[strtok_r](https://www.geeksforgeeks.org/strtok-strtok_r-functions-c-examples/)


[Trouver une fuite de mémoire avec valgrind](https://www.cprogramming.com/debugging/valgrind.html)
