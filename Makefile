.PHONY: all clean

all: villes queue divise

villes: villes.c
	gcc villes.c -o villes

queue: queue.c
	gcc queue.c -o queue

divise: divise.c
	gcc divise.c -o divise

clean:
	rm villes queue divise
